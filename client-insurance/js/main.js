var chatOpened=false;
var newChat;

$(function() {
var options = {
  placeholder : "Type a message...",
  sendButtonTitle : "Send",
  serverUserImage : "https://ces-global-demo-resources.s3.amazonaws.com/converse/pb_logo.png",
  host : "https://new-chatbotdev.ces.pitneycloud.com",
  container : "#chatcontainer",
  showCloseButton : true,
		showSettingsButton: false,
  title : "Chat With Us",
  hideIcon : "glyphicon-remove" // See: https://www.w3schools.com/bootstrap/bootstrap_ref_comp_glyphs.asp
}


newChat = new converseChat(options);
newChat.init(function() {
  $("#addClass").click(function() {
	newChat.show();
  });
  newChat.openBot("sab4a6d", "5be03284-5a86-4836-bde1-9df111f177c7", {
	id : "xxxxxxx",
	first_name : "Friend",
	last_name : "Any",
	full_name : "Customer Any",
	locale : "en_US"
  });
});
 newChat.sendPostBackSilent('auto');
 newChat.show();
})

function openChatbot() {
	if(!chatOpened){
		newChat.clearChat();
		newChat.sendPostBackSilent('auto');
		chatOpened=true;
	}
newChat.show();
}
