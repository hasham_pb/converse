function converseChat(options) {
	this.host = options.host;
	this.baseContaner = options.container;
	this.showCloseButton = typeof options.showCloseButton === "boolean" ? options.showCloseButton : true;
	this.showSettingsButton = typeof options.showSettingsButton === "boolean" ? options.showSettingsButton : true;
	this.sendCallback = options.sendCallback;
	this.receiveCallback = options.receiveCallback;
	this.hideCallback = options.hideCallback;
	this.title = options.title || "Chat";
	this.FADE_TIME = options.FADE_TIME || 150; // ms
	this.REDATE_TIME = options.REDATE_TIME || 10; // minutes
	this.hideIcon = options.hideIcon || "glyphicon-minus";
	this.hideSendButton = options.hideSendButton === true ? true : false ;
	this.placeholder = options.placeholder || "Type a message...";
	this.sendButtonTitle = options.sendButtonTitle || "Send";
	this.serverUserImage = options.serverUserImage || "/public/img/pb-circle-mark.png";
	this.reConnectDelay = options.reConnectDelay || 2000;
	this.disconnectedText = options.disconnectedText || "Disconnected - trying to reconnect";
	this.currentBot;
	this.baseTemplate = "";
	this.socket = io(this.host, {
		reconnectionDelay: 2000,
		extraHeaders: {
            'cookie': document.cookies
        }
  	});
	this.lastTimestamp = 0;
	this.lastDirection = "none";
	this.lastContainer = null;
	this.tryReconnect = false;
};
converseChat.prototype.setReceiveCallback = function(callback) {
	this.receiveCallback = callback;
};
converseChat.prototype.setSendCallback = function(callback) {
	this.sendCallback = callback;
};
converseChat.prototype.init = function(callback) {
	var me = this;
	this.user = {};
	this.socket.on("text", function(message) {
		console.log("text");
		me.text(message);
		if(me.receiveCallback) {
		  me.receiveCallback(message);
		}
	});

	this.socket.on("buttons", function(data) {
		me.addButtonMessage(data);
		if(me.receiveCallback) {
		  me.receiveCallback(data);
		}
	});
	this.socket.on("card", function(data) {
		me.addCardMessage(data);
		if(me.receiveCallback) {
		  me.receiveCallback(data);
		}
	});
	this.socket.on("buttonCard", function(data) {
		me.addCardMessage(data);
		if(me.receiveCallback) {
		  me.receiveCallback(data);
		}
	});
	this.socket.on("quick_replies", function(data) {
		console.log("quick_replies");
		me.addQuckReplyMessage(data);
		if(me.receiveCallback) {
		  me.receiveCallback(data);
		}
	});
	this.socket.on("type_on", me.type_on);
	this.socket.on("type_off", me.type_off);
	this.socket.on("account_link_redirect", me.account_link_redirect);

	this.socket.on("disconnect", function() {
		$("#connectedIcon").addClass("pb-chat-disconnected");
		$("#connection").addClass("pb-chat-disconnected");
		$("#send").prop("disabled", true);
		$("#status_message").prop("disabled", true);
		$("#messagesWrapper").prop("disabled", true);
		$("span").css("pointer-events", "none");
		$(".btn").prop("disabled", true);
		me.socket.close();
		me.retryIntrval = setInterval(function() {
			me.reConnect();
		}, me.reConnectDelay);
	});

	this.socket.on("connect", function() {
		if(me.tryReconnect) {
			me.socket.emit("reestablised", {tenantId: this.tenant, botid: this.bot, user: this.user});
		}
		me.tryReconnect = false;
		if (!this.hideSendButton) {
			$("#send").prop("disabled", false);
		}
		if(me.retryIntrval) {
			clearInterval(me.retryIntrval);
		}
		$("#messagesWrapper").prop("disabled", false);
		$("#status_message").prop("disabled", false);
		$(".btn").prop("disabled", false);
		$("span").css("pointer-events", "auto");
		$("#connectedIcon").removeClass("pb-chat-disconnected");
		$("#connection").removeClass("pb-chat-disconnected");
	});

	this.$baseTemplate = $('<div class="pb-chat-popup-box"></div>');

	var $popHead = $('<div class="pb-chat-popup-head"></div>');
	$popHead.append($('<div class="pb-webchat-title"></div>').text(this.title));

	var $headRight = $('<div class="pb-chat-popup-head-right pull-right"></div>');

	var $btnGroup = $('<div class="btn-group"></div>');
	$btnGroup.append($('<span id="connection" title="'+ this.disconnectedText +'" class="pb-chat-header-connected"><i id="connectedIcon" class="glyphicon glyphicon-flash pb-chat-connected-indicator"></i> </span>'));
	if(this.showSettingsButton) {
		$btnGroup.append($('<button class="pb-chat-header-button" data-toggle="dropdown" type="button" aria-expanded="false"><i class="glyphicon glyphicon-cog"></i> </button>'));
	}

	var $lst = $('<ul role="menu" class="dropdown-menu pull-right"></ul>');
	var $clearChat = $('<li><a href="#">Clear Chat</a></li>');
	$clearChat.click(function() {
		me.clearChat();
	});
	$lst.append($clearChat);
	$btnGroup.append($lst);
	$headRight.append($btnGroup);

	if(this.showCloseButton) {
		$headRight.append($('<button data-widget="remove" id="closeChat" class="pb-chat-header-button pull-right" type="button"><i class="glyphicon ' + this.hideIcon + '"></i></button>'));
	}
	$popHead.append($headRight)

	//this.$baseTemplate.append($popHead);

	var $messageSection = $('<div id="messagesWrapper" class="pb-chat-popup-messages"></div>');
	$messageSection.append($('<div id="messages" class="pb-chat-direct-chat-messages"></div>'));

	this.$baseTemplate.append($messageSection);

	var $footer = $('<div class="pb-chat-popup-messages-footer"></div>');
	$footer.append($('<div id="typing" class="pb-chat-typing-indicator"><span></span><span></span><span></span></div>'));
	var $inputContent = $('<div class="pb-input-content"></div>');
	$inputContent.append($('<textarea id="status_message" placeholder="' + this.placeholder + '" rows="10" cols="40" name="message" ' + (this.hideSendButton ? 'class="fullwidth"' : '') + '></textarea>'));
	if (!this.hideSendButton) {
		$inputContent.append($('<button data-widget="send" id="send" class="pb-chat-send-button" type="button">' + this.sendButtonTitle + '</button>'));
	}
	$footer.append($inputContent);
	this.$baseTemplate.append($footer);
	me.renderMain.call(me, callback);
};
converseChat.prototype.reConnect = function() {
	if (this.socket.disconnected) {
		this.tryReconnect=true;
		this.socket.connect();
		this.socket.open();
	}

};
converseChat.prototype.clearChat = function() {
	$('#messages').empty();
	this.lastTimestamp = 0;
	this.lastDirection = null;
	this.sendPostBackSilent("#silentcancel#");
};

converseChat.prototype.cleanInput = function(input) {
	return $('<div/>').text(input).text();
};

converseChat.prototype.openBot = function(tenant, bot, user) {
	this.tenant = tenant;
	this.bot = bot;
	this.user = user;
	this.socket.emit("openBot", {tenantId: tenant, botid: bot, user: user});
};

converseChat.prototype.text = function(message) {
	this.addChatMessage({
		direction: "inbound",
		message: message
	});
};
converseChat.prototype.type_on = function() {
	$("#typing").addClass("pb-chat-typing-on");
};
converseChat.prototype.type_off = function() {
	$("#typing").removeClass("pb-chat-typing-on");
};
converseChat.prototype.account_link_redirect = function(packet) {
	window.open(packet, "_blank");
};
converseChat.prototype.messageSend = function() {
	var msg = this.cleanInput($("#status_message").val());
	if (msg) {
		this.sendMessage(msg);
		$("#status_message").val('');
	}
};
converseChat.prototype.renderMain = function(callback) {
	var me = this;
	$(me.baseContaner).append(me.$baseTemplate);
	$("#closeChat").click(function () {
		me.hide();
	});
	$("#status_message").keydown(function (event) {
		if (event.which === 13) {
			me.messageSend();
			event.preventDefault();
		}
	});
	$("#send").click(function () {
		me.messageSend();
		$("#status_message").focus();
	});

	if (callback) {
		callback();
	}
};

converseChat.prototype.sendPostBack = function(payload, title) {
	if (payload || title) {
		this.sendPostBackSilent(payload, title);
		this.addChatMessage({
			direction: "outbound",
			message: title
		})
	}
};

converseChat.prototype.sendPostBackSilent = function(payload, title) {
	var send = true;
	if (payload || title) {
		var msg = {
			sender: {id: this.user.id},
			recipient: {id: "FacebookBotClient"},
			timestamp: new Date().valueOf(),
			postback: {
				payload: payload || title
			}
		};
		this.socket.emit("postback", msg);

		if(this.sendCallback) {
			this.sendCallback(payload || title);
		}
	}
};

converseChat.prototype.sendMessage = function(messagetext) {
	var send = true;
	var msg;

	if (messagetext) {
		var msg = {
			sender: {id: this.user.id},
			recipient: {id: "FacebookBotClient"},
			timestamp: new Date().valueOf(),
			message: {
				mid: "some id", // TODO random ids
				text: messagetext
			}
		};

		if(this.sendCallback) {
			if(!this.sendCallback(messagetext)) {
				send = false;
			};
		}
		if (send) {
			this.socket.emit("text", msg);
			this.addChatMessage({
				direction: "outbound",
				message: messagetext
			});
		}
	}
};
converseChat.prototype.show = function() {
 	$(this.baseContaner).addClass('pb-chat-popup-box-on');
};
converseChat.prototype.hide = function() {
	$(this.baseContaner).removeClass('pb-chat-popup-box-on');
	if(this.hideCallback) {
		this.hideCallback();
	}
};
converseChat.prototype.setWrapperItems = function(data) {
	var reset = false;
	/*
	if(this.lastTimestamp < ((new Date()).getTime() - (this.REDATE_TIME * (1000*60)))) {
		this.lastTimestamp = (new Date()).getTime();
		rest = true;
		$("#messages").append($('<div class="pb-chat-box-single-line"><abbr class="timestamp">' + this.getTimeStamp() +'</abbr></div>'));
	}*/

	if (!this.lastContainer || this.lastDirection !== data.direction || reset) {
		var $msgWrapper = $('<div class="pb-chat-direct-chat-msg doted-border pb-' + data.direction + '-container"></div>');
		var $items = $('<div class="pb-' + data.direction + '-items"></div');
		if (data.direction === "inbound") {
			$msgWrapper.append($('<img alt="PitneyBowes" src="'+this.serverUserImage+'" class="pb-chat-direct-chat-img">'));
		}

		$msgWrapper.append($items);
		if (data.direction === "outbound") {
			$msgWrapper.append($('	<i class="glyphicon glyphicon-user pb-chat-direct-chat-outbound-img"></i>'));
			//$msgWrapper.append($('<img alt="PitneyBowes" src="'+this.serverUserImage+'" class="pb-chat-direct-chat-img">'));
		}
		$('#messages').append($msgWrapper);
		this.lastContainer = $msgWrapper;
		this.lastContainerItems = $items;
		this.lastDirection = data.direction;
	}
}

converseChat.prototype.buttonClick = function(button) {
	switch(button.type) {
		case "account_unlink":
			this.socket.emit("account_unlink", {});
			break;
		case "account_link":
			this.socket.emit("init_account_link", { url: button.url });
			break;
		case "web_url":
			window.open(button.url, "_blank");
			break;
		case "postback":
			this.sendPostBack(button.payload, button.title);
			break;
		case "phone_number":
			window.open("tel:" + button.payload, "_self");
			break;
		default:
			this.sendPostBack(button.payload, button.title);
			break;
	}
};
converseChat.prototype.addQuckReplyMessage = function(data, options) {
	data.direction = "inbound";
	var me = this;
	this.setWrapperItems(data);
	var $buttonContainer = $('<div class="pb-chat-direct-chat-text-inbound"></div>');
	var $messageText = $('<div class="pb-chat-button-text"></div>').text(data.text);
	var $messageButtons = $('<div class="pb-chat-quick-reply-container"></div>');

	console.log(data.text);
	$.each(data.quick_replies, function(index, button) {
		var $button = $('<span class="badge pb-chat-badge-brand-info quick-reply">' + button.title + '</span>');
		$button.click(function() {
			me.buttonClick(button);
			$('.pb-chat-quick-reply-container').hide();
		});

		if (button.image_url) {
			$button.append('<img class="quick-reply-img" src="'+button.image_url+'">');
		}
		$messageButtons.append($button);
	});
	$buttonContainer.append($messageText, $messageButtons);
	this.addMessageElement($buttonContainer, data);
};

converseChat.prototype.addCardMessage = function(data, options ) {
	var me = this;
	data.direction = "inbound";
	this.setWrapperItems(data);
	var timestamp = new Date();
	var cardContainerId = "cardContainer" + timestamp.getHours() + "" + timestamp.getMinutes() + "" + timestamp.getSeconds() + "" + timestamp.getMilliseconds();
	var cardContainerStr = "<div class='pb-cards-wrapper'><div class='card-nav-btns show'>";
	var $cardSlider = $("<div id='" + cardContainerId + "' class='slider cards-slider pb-chat-card-container'>");
	var $cardContainer = $(cardContainerStr);

	$.each(data, function(index, card) {

		var $buttonContainer = $('<div class="pb-chat-card"></div>');
		if (card.image_url) {
			$buttonContainer.append('<img class="pb-chat-image" src="' + card.image_url + '">');
		}
		$buttonContainer.append($('<div class="pb-chat-title"></div>').text(card.title));
		if (card.subtitle) {
			$buttonContainer.append($('<div class="pb-chat-subtitle"></div>').text(card.subtitle));
		}
		if (card.item_url) {
			$url = $('<div class="pb-chat-url"></div>').text(card.item_url);
			$url.click(function () { me.buttonClick({type: 'web_url', url: card.item_url}); });
			$buttonContainer.append($url);
		}
		var $cardButtons = $('<div class="pb-chat-button-container"></div>');
		$.each(card.buttons, function(index, button) {
			var $button = $('<button class="btn btn-default pb-chat-btn-live-preview">' + button.title + '</button>');
			$button.click(function() { me.buttonClick(button); });
			$cardButtons.append($button);
		});
		var $card = $buttonContainer.append($cardButtons);
		$cardSlider.append($card);
		$cardContainer.append($cardSlider);
	});

	$cardContainer.append($cardContainer);

	this.addMessageElement($cardContainer, data);

	$(".cards-slider").last().slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		swipe: true,
		variableWidth: true,
		customPaging: function(slider) {
			var currentMsgId = "#" + $(".pb-chat-direct-chat-msg").last().find(".slider").attr("id");
			var currentMsg = $(currentMsgId);
			$(".card-nav-btns, .pb-chat-url, .pb-chat-btn-live-preview, .pb-chat-button-container, .slick-arrow").mouseover(function() {
				if(slider.currentSlide === 0){
					$(currentMsg)
						.find(".slick-next").addClass("show")
						.find(".slick-prev").removeClass("show");
				}
				if(slider.currentSlide > 0){
					$(currentMsg)
						.find(".slick-prev, .slick-next").addClass("show");
				}
				if(slider.currentSlide === (slider.slideCount - 1)){
					$(currentMsg)
						.find(".slick-prev").addClass("show")
						.find(".slick-next").removeClass("show");
				}
			});
			$(".card-nav-btns, .pb-chat-url, .pb-chat-btn-live-preview, .pb-chat-button-container, .slick-arrow").mouseleave(function() {
				$(currentMsg)
					.find(".slick-prev, .slick-next").removeClass("show");
			});
		}
	});
};
converseChat.prototype.addButtonMessage = function(data, options) {
	data.direction = "inbound";
	var me = this;
	this.setWrapperItems(data);
	var $buttonTextContainer = $('<div class="pb-chat-direct-chat-text-inbound"></div>');
	var $buttonContainer = $('<div class="pb-chat-direct-chat-button-inbound "></div>');
	var $messageText = $('<div class="pb-chat-button-text"></div>').text(data.text);
	var $messageButtons = $('<div class="pb-chat-button-container"></div>');
	$.each(data.buttons, function(index, button) {
		var $button = $('<button class="btn btn-default pb-chat-btn-live-preview">' + button.title + '</button>');
		$button.click(function() { me.buttonClick(button); });
		$messageButtons.append($button);
	})
	/*
	$buttonContainer.append($messageText, $messageButtons);
	this.addMessageElement($buttonContainer, data);*/
	$buttonContainer.append($messageButtons);
	$buttonTextContainer.append($messageText);
	this.addMessageElement($buttonTextContainer, data);
	this.addMessageElement($buttonContainer, data);
};

converseChat.prototype.addChatMessage = function(data, options) {
	this.setWrapperItems(data);

	var $messageBodyDiv = $('<div class="pb-chat-direct-chat-text-' + data.direction + ' ">')
		.text(data.message);

	this.addMessageElement($messageBodyDiv, data);
};

converseChat.prototype.addMessageElement = function(el, data) {
	var $el = $(el);
	var $messages = $('#messages');
	$el.hide().fadeIn(this.FADE_TIME);
	this.lastContainerItems.append($el);

	var $items = this.lastContainerItems.find(".pb-chat-direct-chat-text-inbound, .pb-chat-direct-chat-text-outbound, .pb-cards-wrapper");



	$('#messagesWrapper').animate({
		scrollTop: $('#messagesWrapper').get(0).scrollHeight
	}, 1000);

	this.type_off();
};
converseChat.prototype.getTimeStamp = function() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	var HH = today.getHours();
	var MM = today.getMinutes();
	var am = true;
	if(dd<10){dd='0'+dd;}
	if(HH > 12) {am = false;HH = HH - 12;}
	if(HH<10){HH='0'+HH;}
	if(MM<10){MM='0'+MM;}
	if(mm<10){mm='0'+mm;}
	return dd+'/'+mm+'/'+yyyy + ' ' + HH + ':' + MM + (am?'AM':'PM');
};
