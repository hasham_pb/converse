var chatOpened=false;
var newChat;

$(function() {
var options = {
  placeholder : "Type a message...",
  sendButtonTitle : "Send",
  serverUserImage : "https://s3-eu-west-1.amazonaws.com/ces-global-demo-resources/utility/connect/logo-icon.png",
  host : "https://new-chatbotdev.ces.pitneycloud.com",
  container : "#chatcontainer",
  showCloseButton : true,
		showSettingsButton: false,
  title : "Chat With Us",
  hideIcon : "glyphicon-remove" // See: https://www.w3schools.com/bootstrap/bootstrap_ref_comp_glyphs.asp
}


newChat = new converseChat(options);
newChat.init(function() {
  $("#addClass").click(function() {
	newChat.show();
  });
  newChat.openBot("sa9299b", "f29fa09d-dc36-4007-8aa3-c66eb19c28e7", {
	id : "xxxxxxx",
	first_name : "Friend",
	last_name : "Any",
	full_name : "Customer Any",
	locale : "en_US"
  });
});

})

function openChatbot() {
	if(!chatOpened){
		newChat.clearChat();
		newChat.sendPostBackSilent('start&Mike&$333&$333&2018-06-01&http://google.de');
		chatOpened=true;
	}
newChat.show();
}