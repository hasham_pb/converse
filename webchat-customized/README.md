## Converse advance integration

This sample shows how you can customize the default Converse client library.

The steps are:

1. Download converseChatV2.js and converseChatV2.css and add them to your project
2. Add in your index.html the link to the downloaded files: 
    ```
    <link rel="stylesheet" href="css/converse.css">
    <script  src="js/converse.js"></script>
    ```
   
3. Change the Converse files according to your needs
4. Additional files needed for Converse are:
```
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css">
```
