var chatOpened=false;
var newChat;

$(function() {
var options = {
  placeholder : "Type a message...",
  sendButtonTitle : "Send",
  serverUserImage : "https://ces-global-demo-resources.s3.amazonaws.com/converse/pb_logo.png",
  host : "https://new-chatbotdev.ces.pitneycloud.com",
  container : "#chatcontainer",
  showCloseButton : true,
		showSettingsButton: false,
  title : "Chat With Us",
  hideIcon : "glyphicon-remove" // See: https://www.w3schools.com/bootstrap/bootstrap_ref_comp_glyphs.asp
}


newChat = new converseChat(options);
newChat.init(function() {
  $("#addClass").click(function() {
	newChat.show();
  });
  newChat.openBot("sa9299b", "eeb3ea13-e02f-4ab1-ba3a-da0c782f9c7e", {
	id : "xxxxxxx",
	first_name : "Friend",
	last_name : "Any",
	full_name : "Customer Any",
	locale : "en_US"
  });
});
 newChat.sendPostBackSilent('start');
 newChat.show();
})

function openChatbot() {
	if(!chatOpened){
		newChat.clearChat();
		newChat.sendPostBackSilent('start&Mike&$333&$333&2018-06-01&http://google.de');
		chatOpened=true;
	}
newChat.show();
}
